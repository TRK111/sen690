<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
	    error_reporting(1);

	    $ttl = 0;
        $fp = fopen('assets/rds2/logs_w.csv', 'a');

        $number_of_days = date('t',strtotime('2021-01-01'));
        $sl = 7;
        for ($n=$sl;$n<=$sl;$n++)
        {
            $n = sprintf("%02d",$n);

            echo $n.'<br>';

            $data = array_map('str_getcsv', file('assets/rds2/2021-01-'.$n.'.csv'));

            for ($i=0;$i<count($data);$i++)
            {
                $line_data = $data[$i][0];

                $row_array = explode('-->',$line_data);

                $data1 = ($row_array[0]);
                $data1Explode = explode('-',$data1);
                $firstColData = trim($data1Explode[0]);

                if($firstColData == "ERROR")
                {

                    $data2 = ($row_array[1]);
                    $data2Explode = explode(':',$data2);
                    $secondColData = isset($data2Explode[1]) ? trim($data2Explode[1]) : '';

                    if (strpos($row_array[2], 'stdClass::') !== false) {
                        $data3 = str_replace("stdClass::","",$row_array[2]);
                    }
                    elseif (strpos($row_array[2], 'non-object') !== false) {
                        $data3 = str_replace("non-object","non-object : ",$row_array[2]);
                    }
                    elseif (strpos($row_array[2], 'foreach()') !== false) {
                        $data3 = str_replace("foreach()","foreach() : ",$row_array[2]);
                    }


                    $thirdColData = trim($data3);

                    $data3Explode = explode(':',$data3);

                    if (isset($data3Explode[1]))
                    {
                        $fourthData = $data3Explode[0];
                        $fifthData  = $data3Explode[1];

                        $fifthDataArray = explode(' ',$fifthData);

                    }
                    else
                    {
                        $fourthData = $fifthDataArray[1] = $fifthDataArray[2] = $fifthDataArray[3] = '';
                    }

                    if(!isset($fifthDataArray[1]))
                    {
                        $fifthDataArray[1] = $fifthDataArray[2] = $fifthDataArray[3] = '';
                    }


                    //$a = array($firstColData,$secondColData,$thirdColData,$fourthData,$fifthDataArray[1],$fifthDataArray[2],$fifthDataArray[3]);
                    $a = array($firstColData,$secondColData,$fourthData,$fifthDataArray[1],$fifthDataArray[2],$fifthDataArray[3]);


                    fputcsv($fp, $a, ",");

                }
                $ttl++;
                //echo 'assets/2021-01-'.$n.'.csv';
            }

            echo "<h1 align='center' style='color: green'><u>$i</u> Rows Done</h1>";
        }

        echo "<h1 align='center' style='color: rebeccapurple'><u>Total:</u> $ttl </h1>";
	}

}
